<?php get_header(); ?>

<!-- BEGIN OF page.php -->

  <?php the_post(); ?>
  <div class='article articlebody'>
    <h2><?php the_title(); ?></h2>
    <!-- Start of the_content() -->
    <?php the_content(); ?>
    <!-- End of the_content() -->
  </div>

<!-- END OF page.php -->

<?php get_footer(); ?>
