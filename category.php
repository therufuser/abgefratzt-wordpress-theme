<!-- BEGIN OF category.php -->

<?php get_header(); ?>
  <h2 class='centered'>Unsere jüngsten Podcasts</h2>
  <div class='podcastlist'>
    <?php if ( have_posts() ) : ?> <!-- Display all posts/articles/news -->
      <?php while(have_posts()): the_post(); ?>
        <div class='podcastshort'>
          <?php the_post_thumbnail(); ?>
          <div class='podcastheader'>
            <h3><?php the_title(); ?></h3>
            <p><?php the_time('d.m.y') ?></p>
          </div>
          <div class='description'><?php the_excerpt(); ?></div>
          <?php the_shortlink("<img src='" . get_stylesheet_directory_uri() . "/images/pfeil_rot_rechts.png' alt='zum Podcast'>");  ?>
        </div>
      <?php endwhile; ?>
    <?php else: ?> <!-- Don't have anything to show -->
      <p>nix zu sehen..</p>
    <?php endif; ?>
  </div>
<?php get_footer(); ?>

<!-- END OF category.php -->
