<!-- BEGIN OF header.php -->

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class='header'>
		<!-- Display WordPress-Widgets if there are any -->
		<div class='widgets'>
			<?php dynamic_sidebar('header-widgets'); ?>
		</div>

    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img id='fakeheader' src='<?php echo get_stylesheet_directory_uri(); ?>/images/header.png' alt='Headergrafik, zurück zur Startseite'>
		</a>
  </div>

	<!-- Use a WordPress menu for the navigation bar -->
  <div class='navigation'>
		<?php wp_nav_menu( array( 'theme_location' => 'navigation-menu' ) ); ?>
  </div>
<!-- END OF header.php -->
