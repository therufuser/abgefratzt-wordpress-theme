<?php

function theme_setup() {
  add_theme_support('post-thumbnails');
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'theme_setup');

function register_menus() {
  register_nav_menu('navigation-menu', __('Navigation'));
  register_nav_menu('footer-menu-left', __('Footer Left'));
  register_nav_menu('footer-menu-right', __('Footer Right'));
}
add_action('init', 'register_menus');

function register_widget_areas() {
  $header_widgets = array(
    'name'          => 'Header Area',
    'id'            => 'header-widgets',
    'description'   => 'Space for widgets in the header',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>',
  );

  $footer_widgets = array(
    'name'          => 'Footer Area',
    'id'            => 'footer-widgets',
    'description'   => 'Space for widgets in the footer',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  );

  register_sidebar($header_widgets);
  register_sidebar($footer_widgets);
}
add_action('widgets_init', 'register_widget_areas');
