<?php get_header(); ?>

<!-- BEGIN OF single.php -->

  <?php the_post(); ?>
  <div class='podcastlong'>
    <h2><?php the_title(); ?></h2>
    <div class='podcastinfolarge'>
      <p><?php the_time('d.m.y'); ?></p>
    </div>
    <?php if(has_post_thumbnail()): ?>
      <div class='podcastimage'>
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>
    <p class='description'><?php the_content(); ?></p>
    <a class='listenbutton' href='<?php echo get_post_meta(get_the_ID(), 'listen-link', true); ?>'>Reinhören!</a>

    <?php the_author_posts_link(); ?>

    <dl>
      <dt>Redaktion</dt>
      <dd><?php the_author_link(); ?></dd>
    </dl>

    <?php
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;
    ?>
  </div>

<!-- END OF single.php -->

<?php get_footer(); ?>
