<!-- BEGIN OF footer.php -->
  <div class='footer'>
		<!-- Display WordPress-Widgets if there are any -->
		<div class='widgets'>
			<?php dynamic_sidebar('footer-widgets'); ?>
		</div>

		<!-- Use a WordPress menu for the footer -->
		<div class='menu menu-left'>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-left' ) ); ?>
		</div>

		<!-- Show who owns us -->
		<a href='https://wordpress.org/' class='imprint'>
			Proudly powered by WordPress
		</a>

		<!-- Use a WordPress menu for the footer -->
		<div class='menu menu-right'>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-right' ) ); ?>
		</div>
	</div>

	<?php wp_footer(); ?>
</body>
</html>

<!-- END OF footer.php -->
