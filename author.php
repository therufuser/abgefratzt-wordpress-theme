<!-- BEGIN OF author.php -->

<?php get_header(); ?>
  <div><h2 class='centered'>
    <?php echo get_avatar(get_the_author_meta('user_email'), $size = '50'); ?>
    Post by <?php the_author(); ?></h2>
    <p><?php the_author_meta('description'); ?></p></div>
  <div class='podcastlist'>
    <?php if ( have_posts() ) : ?> <!-- Display all posts/articles/news -->
      <?php while(have_posts()): the_post(); ?>
        <div class='podcastshort'>
          <?php the_post_thumbnail(); ?>
          <div class='podcastheader'>
            <h3><?php the_title(); ?></h3>
            <p><?php the_time('d.m.y') ?></p>
          </div>
          <div class='description'><?php the_excerpt(); ?></div>
          <?php the_shortlink("<img src='" . get_stylesheet_directory_uri() . "/images/pfeil_rot_rechts.png' alt='zum Podcast'>");  ?>
          <!-- a href='chor_ostseeschule.html'><img src='/images/pfeil_rot_rechts.png' alt='zum Podcast'></a -->
        </div>
      <?php endwhile; ?>
    <?php else: ?> <!-- Don't have anything to show -->
      <?php get_template_part( 'content', 'none' ); ?>
    <?php endif; ?>
  </div>
<?php get_footer(); ?>

<!-- END OF author.php -->
