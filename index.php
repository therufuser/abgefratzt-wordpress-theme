<!-- BEGIN OF index.php -->

<?php get_header(); ?>
  <div class='mainpage'>
    <div class='podcastlist'>
      <h4>Aktuelle Podcasts:</h2>
      <?php $podcast_query = new WP_Query(array('category_name' => 'audioarchive')); ?>
      <?php //query_posts(array('category_name' => 'audioarchive')); ?>
      <?php while($podcast_query->have_posts()): $podcast_query->the_post(); ?>
        <div class='podcastshort'>
          <?php the_post_thumbnail(); ?>
          <div class='podcastheader'>
            <h3><?php the_title(); ?></h3>
            <p><?php the_time('d.m.y'); ?></p>
          </div>
          <?php the_shortlink("<img src='" . get_stylesheet_directory_uri() . "/images/pfeil_rot_rechts.png' alt='zum Podcast'>");  ?>
        </div>
			<?php endwhile; ?>
      <?php wp_reset_postdata() ?>
    </div>

    <div class='article'>
      <h2>Neues</h2>
      <?php $news_query = new WP_Query(array('category_name' => 'news')); ?>
      <?php //query_posts(array('category_name' => 'news')); ?>
      <?php if ($news_query->have_posts()) : ?> <!-- Display all posts/articles/news -->
        <?php while($news_query->have_posts()): $news_query->the_post(); ?>
          <div class='articlebody'>
            <h3 id='introduction'><?php the_title(); ?></h2>
            <dl>
              <dt>Veröffentlicht am</dt>
              <dd><?php the_time('d.m.y') ?></dd>
            </dl>
            <?php the_content(); ?>
          </div>
  			<?php endwhile; ?>
  		<?php else: ?> <!-- Don't have anything to show -->
  			<p>Gibt grad leider nix neues... Irgendwann vielleicht</p>
      <?php endif; ?>
      <?php wp_reset_postdata() ?>
    </div>
  </div>
<?php get_footer(); ?>

<!-- END OF index.php -->
